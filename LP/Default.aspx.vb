﻿Imports System.IO
Imports System.Net.Mail
Imports System.Data
Imports MySql.Data.MySqlClient

Public Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub btEnviar_Click(sender As Object, e As EventArgs) Handles btEnviar.Click

        gravaContatos()

    End Sub

    Sub gravaContatos()

        'Variáveis
        Dim sql As String
        Dim cmd As MySqlCommand

        'Define Conexão
        Dim Site As New MySqlConnection(ConfigurationManager.ConnectionStrings("ConnWebsites").ConnectionString)

        Try

            'Abre conexão
            If Site.State = ConnectionState.Closed Then Site.Open()

            'Define instrução SQL
            sql = "insert into tab_contatos (cliente, campanha, nome, email, telefone, descricao, data) values (?cliente, ?campanha, ?nome, ?email, ?telefone, ?descricao, ?data);"

            'Define Command
            cmd = New MySqlCommand(sql, Site)

            cmd.Parameters.AddWithValue("?cliente", "Nilay")
            cmd.Parameters.AddWithValue("?campanha", "Google AdWords")
            cmd.Parameters.AddWithValue("?nome", Me.txNome.Text)
            cmd.Parameters.AddWithValue("?email", Me.txEmail.Text)
            cmd.Parameters.AddWithValue("?telefone", Me.txTelefone.Text)
            cmd.Parameters.AddWithValue("?descricao", Me.txDescricao.Text)
            cmd.Parameters.AddWithValue("?data", Convert.ToDateTime(FormatDateTime(Now)))

            'Executa
            cmd.ExecuteNonQuery()

            'Libera conexão
            cmd.Dispose()

            'Fecha a Conexão
            Site.Dispose()
            Site.Close()
            Site = Nothing

        Catch ex As Exception

            'Fecha a Conexão
            Site.Dispose()
            Site.Close()
            Site = Nothing

        End Try

        'Envia o contato para o administrador
        enviaContato()

    End Sub

    Sub enviaContato()

        'Encode para brasileiro
        Dim encode As System.Text.Encoding
        encode = System.Text.Encoding.GetEncoding("iso-8859-1")

        'Objeto do Email
        Dim email As New MailMessage

        'Le o email criado
        Dim objReader As New StreamReader(Server.MapPath(".") & "\fale-conosco.html", encode)

        'Le o arquivo até o fim
        Dim corpo As String = objReader.ReadToEnd()
        corpo = Replace(corpo, "[#nome#]", Me.txNome.Text)
        corpo = Replace(corpo, "[#email#]", Me.txEmail.Text)
        corpo = Replace(corpo, "[#telefone#]", Me.txTelefone.Text)
        corpo = Replace(corpo, "[#descricao#]", Me.txDescricao.Text)
        corpo = Replace(corpo, "[#assinatura#]", ConfigurationManager.AppSettings("Assinatura"))
        corpo = Replace(corpo, "[#caminho#]", ConfigurationManager.AppSettings("bsHtml"))
        corpo = Replace(corpo, "[#cliente#]", ConfigurationManager.AppSettings("Cliente"))
        corpo = Replace(corpo, "[#data#]", FormatDateTime(Now, DateFormat.LongDate))

        'Fecha e destroi a conexão
        objReader.Close()
        objReader = Nothing

        ' cria uma instância do objeto MailMessage
        Dim mMailMessage As New MailMessage()

        ' Define o endereço do remetente
        mMailMessage.From = New MailAddress(ConfigurationManager.AppSettings("contato"), ConfigurationManager.AppSettings("Cliente"))

        ' Define o destinario da mensagem
        mMailMessage.To.Add(New MailAddress("contato@madhuspa.com.br", ConfigurationManager.AppSettings("Cliente")))
        mMailMessage.Bcc.Add(New MailAddress("enrico@suaempresa.net", ConfigurationManager.AppSettings("Cliente")))

        mMailMessage.ReplyToList.Add(Me.txEmail.Text)

        ' Define o assunto 
        mMailMessage.Subject = "Contato pelo site (" & Me.txNome.Text & ")"

        ' Define o corpo da mensagem
        mMailMessage.Body = corpo

        ' Define o formato do email como HTML
        mMailMessage.IsBodyHtml = True

        ' Define a prioridade da mensagem como normal
        mMailMessage.Priority = MailPriority.Normal

        ' Obtain the Network Credentials from the mailSettings section
        Dim credential As New System.Net.NetworkCredential( _
      "suaempresanetsmtp@gmail.com", "noreply4578")

        'Create the SMTP Client
        Dim client As New SmtpClient()
        client.Host = "smtp.gmail.com"
        client.Port = "587"
        client.EnableSsl = True
        client.Credentials = credential

        'Envia Email por SMTP
        client.Send(mMailMessage)


        Response.Redirect("agradecimento.aspx")
        'Me.phForm.Visible = False
        'Me.phSucesso.Visible = True

    End Sub

End Class