﻿<%@ Page Title="Madhu | Spa Massagem" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Principal.Master" CodeBehind="agradecimento.aspx.vb" Inherits="JJVideos.agradecimento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="Obrigado por entrar em contato com a Madhu Spa Massagem" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="row0">
        <div class="row mw960">
            <div class="col-md-4 col-xs-12">
                <a id="logo" href="/">
                    <img src="/img/logo-lp.png" alt="Madhu Spa Massagem" /></a>
            </div>
            <div class="col-md-8 col-xs-12">
                <div id="linha1Header">
                    <p>Com uma equipe diferenciada de terapeutas, você conta com várias opções de massagens. Ligue agora para:</p>
                </div>
                <div id="linha2Header"><span>WhatsApp:</span> <a href="tel:11953236224" rel="nofollow">(11) 95323-6224</a> / <a href="tel:1125066290" rel="nofollow">(11) 2506-6290</a></div>
            </div>
        </div>
    </div>
    <div id="row1">
        <div class="row mw960">
            <div id="row1Col1" class="col-xs-12 col-md-7 cFFF pt40">
            </div>
            <div class="col-xs-12 col-md-5">
                <section id="formCtn">
                    <p id="chamada">
                        <img src="/img/whatsapp.png" alt="Whatsapp" />
                        <span style="color: #6ed17b;">WhatsApp:</span><br />
                        <a href="tel:11953236224" rel="nofollow"><span style="color: #FFF;">(11) 95323-6224</span></a>
                    </p>
                    <div id="setaForm"></div>
                    <div id="camposBotaoCtn">
                        <p>
                            Obrigado!
                            <br />
                            <br />
                            Em breve entraremos em contato.
                        </p>
                        <!-- Global site tag (gtag.js) - Google AdWords: 828161881 -->
                        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-828161881"></script>
                        <script>
                            window.dataLayer = window.dataLayer || [];
                            function gtag() { dataLayer.push(arguments); }
                            gtag('js', new Date());

                            gtag('config', 'AW-828161881');
                        </script>
                    </div>
                    <div id="acabamentoForm"></div>
                </section>
            </div>
        </div>
    </div>
    <div id="row1b">
        <div class="row mw960">
            <div class="col-xs-12">
                <div id="enderecoRow">
                    Rua Emílio Mallet, 219, Tatuapé, São Paulo / SP CEP 03320-001<br />
                    Segunda à Sexta: das 9h00 até as 21h30<br />
                    Sábado, Domingo e Feriados: das 10h00 até as 18h00
                </div>
            </div>
        </div>
    </div>
    <div id="row2">
        <div class="row mw960">
            <section id="row2Col1" class="col-xs-12">
                <h1>Conheça nossa Massagem Relaxante</h1>
                <p class="headerIntro">A massagem relaxante proporciona o alívio do stress e das dores. São realizadas manobras terapêuticas específicas como amassamento, deslizamentos e percussões. Melhora a circulação sanguínea, aumenta o fluxo de nutrientes, além de aliviar a dor e facilitar a atividade muscular.</p>
                <div class="divisor"></div>
                <div class="row">
                    <div class="col-xs-6 col-md-3 xsMb20">
                        <span class="fa-stack">
                            <span class="fa fa-circle fa-stack-2x"></span>
                            <span class="fa fa fa-car fa-stack-1x"></span>
                        </span>
                        <div class="beneficio">
                            <span>Estacionamento</span><br />
                            <span>no Local</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3 xsMb20">
                        <span class="fa-stack">
                            <span class="fa fa-circle fa-stack-2x"></span>
                            <span class="fa fa-users fa-stack-1x"></span>
                        </span>
                        <div class="beneficio">
                            <span>Atendimento</span><br />
                            <span>Exclusivo</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <span class="fa-stack">
                            <span class="fa fa-circle fa-stack-2x"></span>
                            <span class="fa fa-home fa-stack-1x"></span>
                        </span>
                        <div class="beneficio">
                            <span>Salas</span><br />
                            <span>Climatizadas</span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <span class="fa-stack">
                            <span class="fa fa-circle fa-stack-2x"></span>
                            <span class="fa fa-tint fa-stack-1x"></span>
                        </span>
                        <div class="beneficio">
                            <span>Óleos 100%</span><br />
                            <span>Orgânicos</span>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="row mw960">
            <section id="row2Col2" class="col-xs-12">
                <h1>Massagem Relaxante</h1>
                <div class="divisor divisorB"></div>
                <p class="headerIntro">
                    A massagem relaxante tem o intuito de melhorar a circulação sanguínea, aumentar o fluxo de nutrientes, além de aliviar a dor e facilitar a atividade muscular; proporcionando reduções significativas nos níveis do hormônio do stress (cortisol) e provocando elevações no hormônio associado ao contentamento e à confiança que provocam a sensação de bem-estar e relaxamento.
                </p>
            </section>
        </div>
        <section id="row2Col3" class="mw960">
            <div class="row ">
                <div class="col-xs-12 col-md-6">
                    <div class="barraEsquerda">
                        <h1>Terapeutas em Destaque!</h1>
                    </div>
                    <p class="headerIntro">Conheça um pouco mais da nossa equipe de terapeutas</p>
                    <div class="divisor hidden-md"></div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <img id="aparelho" src="/img/chat.jpg" alt="Equipe" />
                </div>
            </div>
            <div class="row terapeutaRow">
                <div class="col-xs-12 col-md-6">
                    <img src="Img/terapeuta-destaque-01.jpg" />
                </div>
                <div class="col-xs-12 col-md-6">
                    <h2>Karol</h2>
                    <p>Sou massoterapeuta formada e especializada para realizar uma massagem bem feita onde trabalhar o corpo é uma das principais maneiras de promover o bem estar físico e mental das pessoas.</p>
                    <p>Quem é que nunca se sentiu cansado e irritado no final de um dia corrido?</p>
                    <p>Situações de tensão acontecem frequentemente na vida de todos nós, para aliviar a tensão e o stress nada melhor do que relaxar recebendo uma massagem, além de livrar o corpo das tensões, a massagem também ajuda a amenizar alguns desequilíbrios como dores, fadiga e má postura, restabelece a saúde física e mental. <strong>Reserve um tempo para você!</strong></p>
                </div>
            </div>
            <div class="row terapeutaRow" style="margin-bottom: 0;">
                <div class="col-xs-12 col-md-6">
                    <img src="Img/terapeuta-destaque-02.jpg" />
                </div>
                <div class="col-xs-12 col-md-6">
                    <h2>Jessica</h2>
                    <p>Sou terapeuta corporal com formação em diversas técnicas de massagem e participação em vários cursos livres de massoterapia, e tenho como objetivo aplicar princípios, métodos e técnicas de terapias naturais com a finalidade de manter, equilibrar ou restabelecer a saúde, a harmonia e a qualidade de vida através do corpo.</p>
                    <p>Algumas das técnicas aplicadas são Quick Massagem, Relaxante, Pedras quentes (Senac), entre outras modalidades.</p>
                    <p><strong>Dedique um tempo para seu bem estar!</strong></p>
                </div>
            </div>
        </section>
        <iframe id="mapa" class="mw960 center-block" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4349.632890411924!2d-46.57448412540502!3d-23.547219158425474!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5e9500214c8f%3A0x8aa57d855b24c5b5!2sR.+Em%C3%ADlio+Mallet%2C+219+-+Vila+Gomes+Cardim%2C+S%C3%A3o+Paulo+-+SP%2C+03320-001!5e0!3m2!1spt-BR!2sbr!4v1509988289421" frameborder="0" allowfullscreen></iframe>
    </div>
    <div id="row3">
        <section class="row mw960">
            <h1>WhatsApp: <a href="tel:11953236224" rel="nofollow"><span style="color: #FFF;">(11) 95323-6224</span></a> ou <a href="tel:1125066290" rel="nofollow"><span style="color: #FFF;">(11) 2506-6290</span></a></h1>
            <p class="chamada">Você merece um momento de relaxamento e bem-estar!</p>
            <div class="divisor"></div>
            <a id="enviarBtnFoot" href="#" class="btn btn-default btn-block">Agendar Horário</a>
        </section>
    </div>
</asp:Content>
